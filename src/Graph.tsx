import React, { useState } from "react";
import { useCollectionOnce } from "react-firebase-hooks/firestore";
import * as firebase from "firebase/app";
import "firebase/firestore";
import { app } from "./ts/firebaseapp";
import { Chart } from "react-google-charts";
import DateRangePicker from "@wojtekmaj/react-daterange-picker";

const now = new Date();
const lastMidnight = new Date(now.getFullYear(), now.getMonth(), now.getDate());
const nextMidnight = new Date(lastMidnight.getTime() + 86399999);

export function Graph() {
  const [divider, setDivider] = useState(15);
  const [date, setDate] = useState([lastMidnight, nextMidnight]);

  const db = firebase.firestore(app);
  const docRef = db
    .collection("data")
    .doc("templogger_1")
    .collection("sensor_data")
    .orderBy("timestamp", "desc")
    .limit(2016)
    .where("timestamp", ">=", date[0].toISOString())
    .where("timestamp", "<=", date[1].toISOString());

  var handleDate = (date: Date[]) => {
    setDate(date);
  };
  var handleResolution = (e: any) => {
    setDivider(e.target.value);
  };

  let datePickerProps = {
    locale: "fi",
    maxDate: nextMidnight,
    minDate: new Date("2020-04-20"),
    calendarType: "ISO 8601",
    showWeekNumbers: true,
    showNeighboringMonth: false,
    returnValue: "range",
    clearIcon: null,
  };

  const [collection, loading, err] = useCollectionOnce(docRef);

  if (err) {
    return <div>{err}</div>;
  }
  if (loading) {
    return <div>loading</div>;
  }
  if (collection) {
    let sensor_data: any = [];
    collection.docs.forEach((c: any) => {
      sensor_data.push(c.data());
    });

    let values: string[] = sensor_data.map((d: any) => {
      let mins = parseInt(d.timestamp.slice(14, 16));
      if (mins % divider === 0) {
        var data = {
          timestamp: new Date(d.timestamp),
          temp: d.temp,
          humidity: d.humidity,
        };
        return Object.values(data);
      } else return null;
    });

    values = values.filter((x) => x !== null);
    let rows: any = values;

    return (
      <div className="graph">
        <DateRangePicker
          {...datePickerProps}
          onChange={handleDate}
          value={date}
        />
        <select
          onChange={handleResolution}
          id="resolution"
          name="resolution"
          defaultValue={divider}
        >
          <option value="5">5 min</option>
          <option value="15">15 min</option>
          <option value="30">30 min</option>
          <option value="60">60 min</option>
        </select>
        <Chart
          chartLanguage="fi"
          width={640}
          height={480}
          chartType="LineChart"
          columns={[
            { type: "datetime", label: "timestamp" },
            { type: "number", label: "Temp \xb0C" },
            { type: "number", label: "RH %" },
          ]}
          rows={rows}
          loader={<div>loading chart</div>}
          options={{
            title: "Temperature and relative humidity by date",
            series: {
              1: { curveType: "function" },
            },
          }}
        />
      </div>
    );
  }

  return null;
}
