import React from "react";
import { useDocument } from "react-firebase-hooks/firestore";
import * as firebase from "firebase/app";
import "firebase/firestore";
import { app } from "./ts/firebaseapp";

export function Latest(props: any) {
  let dateTimeFormat: Intl.DateTimeFormat = props.format;

  const db = firebase.firestore(app);
  const docRef = db
    .collection("data")
    .doc("templogger_1")
    .collection("last_reading")
    .doc("reading");
  const [doc, loading, err] = useDocument(docRef);

  if (err) {
    return <div>{err.message}</div>;
  }
  if (loading) {
    return <div>loading document</div>;
  }
  if (doc) {
    let data = doc.data();
    if (data) {
      type MyData = {
        temp: number;
        humidity: number;
        timestamp: string;
      };
      let myData: MyData = Object.assign(data);
      const { temp, humidity, timestamp } = myData;

      return (
        <div>
          <h3>Current reading</h3>
          <div className="current">
            <div id="timestamp">
              {dateTimeFormat.format(new Date(timestamp))}
            </div>
            <div id="temp">
              Temp: <strong>{temp} &#xb0;C</strong>
            </div>
            <div id="humidity">
              RH: <strong>{humidity} %</strong>
            </div>
          </div>
          {/*
          <table>
            <thead>
              <tr>
                <th id="temp">Temp &#xb0;C</th>
                <th id="humidity">RH %</th>
                <th id="timestamp">Measure timestamp</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{temp.toFixed(1)} &#xb0;C</td>
                <td>{humidity.toFixed(1)} %</td>
                <td>{dateTimeFormat.format(new Date(timestamp))}</td>
              </tr>
            </tbody>
          </table>
          */}
        </div>
      );
    }
    return <div>No data inside document</div>;
  }
  return <div>No document</div>;
}
