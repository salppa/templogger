import React, { useState } from "react";
import DatePicker, { DatePickerProps } from "react-date-picker"; // eslint-disable-line
import * as firebase from "firebase/app";
import "firebase/auth";
import { useAuthState } from "react-firebase-hooks/auth";
import { Login } from "./Login";
import { Latest } from "./Latest";
import { History } from "./History";
import { Graph } from "./Graph";
import { app } from "./ts/firebaseapp";
import "./style/app.css";

function App() {
  var dateTimeFormat = Intl.DateTimeFormat(["fi", "en-US"], dateTimeOpts);

  const now = new Date();
  const lastMidnight = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate()
  );
  const nextMidnight = new Date(lastMidnight.getTime() + 86399999);

  const [date, setDate] = useState([lastMidnight, nextMidnight]);
  const [user, loading, err] = useAuthState(firebase.auth(app));
  const [page, setPage] = useState("home");

  var handleDate = (date: any) => {
    setDate(date);
  };

  var handlePage = (e: any) => {
    setPage(e.target.id);
  };

  function renderTables(
    user?: firebase.User,
    loading?: boolean,
    err?: firebase.auth.Error
  ) {
    if (err) {
      return (
        <div>
          {err.code}: {err.message}
        </div>
      );
    }
    if (loading) {
      return <div>loading...</div>;
    }
    if (user) {
      return (
        <div>
          <div className="head">
            <div id="home" onClick={handlePage}>
              Home
            </div>
            <div id="table" onClick={handlePage}>
              Table
            </div>
            <div id="graph" onClick={handlePage}>
              Graph
            </div>
            <div id="logout" onClick={() => firebase.auth().signOut()}>
              Logout
            </div>
          </div>
          <Latest format={dateTimeFormat} />
          {renderPage()}
        </div>
      );
    }

    return (
      <div>
        <div>Please login to view data</div>
      </div>
    );
  }

  function renderPage() {
    if (page === "home") {
      return null;
    }
    if (page === "table") {
      return (
        <div>
          <h3>History</h3>
          Select date: {renderDatePicker()}
          <History date={date} format={dateTimeFormat} />
        </div>
      );
    }
    if (page === "graph") {
      return (
        <div>
          <h3>Graph</h3>
          <Graph />
        </div>
      );
    }
  }

  function renderDatePicker() {
    let datePickerProps: DatePickerProps | undefined = {
      locale: "fi",
      maxDate: nextMidnight,
      minDate: new Date("2020-01-01"),
      calendarType: "ISO 8601",
      showWeekNumbers: true,
      showNeighboringMonth: false,
      returnValue: "range",
      clearIcon: null,
    };
    return (
      <DatePicker {...datePickerProps} onChange={handleDate} value={date} />
    );
  }

  return (
    <div id="app">
      {renderTables(user, loading, err)}
      <Login />
    </div>
  );
}

const dateTimeOpts: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  second: "2-digit",
  hour12: false,
  timeZone: "Europe/Helsinki",
  timeZoneName: "short",
};

export default App;
