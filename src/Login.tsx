import React, { useState, ChangeEvent } from "react"; // eslint-disable-line
import { useAuthState } from "react-firebase-hooks/auth";
import { app } from "./ts/firebaseapp";
import * as firebase from "firebase/app";
import "firebase/auth";

export function Login() {
  const [creds, setCreds] = useState({ email: "", password: "" });
  const [user, loading] = useAuthState(firebase.auth(app));
  const login = async () => {
    try {
      firebase
        .auth(app)
        .signInWithEmailAndPassword(creds.email, creds.password)
        .then(() => {
          setCreds({ email: "", password: "" });
        })
        .catch((err) => {
          if (err) {
            alert(err);
          }
        });
    } catch (err) {
      alert(err);
    }
    return <p>Loading....</p>;
  };
  const loginWithGoogle = async () => {
    try {
      let provider = new firebase.auth.GoogleAuthProvider();
      firebase
        .auth(app)
        .signInWithPopup(provider)
        .catch((err) => {
          if (err) {
            alert(err);
          }
        });
    } catch (err) {
      alert(err);
    }
    return <p>Loading...</p>;
  };

  if (loading) {
    return (
      <div>
        <p>waiting for auth...</p>
      </div>
    );
  }
  if (user) {
    return null;
  }
  var handleChange = (evt: ChangeEvent<HTMLInputElement>) => {
    if (evt.target.name === "email") {
      setCreds({ email: evt.target.value, password: creds.password });
    }
    if (evt.target.name === "password") {
      setCreds({ email: creds.email, password: evt.target.value });
    }
  };
  return (
    <div>
      <form onSubmit={(e) => e.preventDefault()}>
        <input
          required
          type="email"
          name="email"
          placeholder="email"
          onChange={handleChange}
          value={creds.email}
        ></input>
        <input
          required
          type="password"
          name="password"
          placeholder="password"
          onChange={handleChange}
          value={creds.password}
        ></input>
        <p>
          <button onClick={login}>Login</button>
        </p>
      </form>
      <p>
        <button onClick={loginWithGoogle}>Login with Google</button>
      </p>
    </div>
  );
}
