import React, { useState } from "react";
import { useCollectionOnce } from "react-firebase-hooks/firestore";
import * as firebase from "firebase/app";
import "firebase/firestore";
import { app } from "./ts/firebaseapp";

export function History(props: any) {
  let dateTimeFormat: Intl.DateTimeFormat = props.format;
  let date: DateMinMax = {
    min: props.date[0],
    max: props.date[1],
  };

  const [options, setOptions] = useState(collectionOptions);
  const [divider, setDivider] = useState(15);

  let doclimit = options.limit;
  if (options.limit >= 290) {
    doclimit = 290;
  }

  var handleLimit = (e: any) => {
    setOptions({
      limit: parseInt(e.target.value),
      document: options.document,
      direction: options.direction,
    });
  };

  var handleRefresh = () => {
    var docRef = applyFilter(options, doclimit, date);
    setFilter(docRef);
  };

  var handleResolution = (e: any) => {
    setDivider(e.target.value);
  };

  const [filter, setFilter] = useState<firebase.firestore.Query>();
  const [collection, loadingCollection, errCollection] = useCollectionOnce(
    filter
  );

  if (errCollection) {
    console.error(errCollection);
    return (
      <div>
        {errCollection.name}: {errCollection.message}
      </div>
    );
  }
  if (loadingCollection) {
    return <div>Loading document</div>;
  }
  if (collection) {
    let sensor_data: any = [];
    collection.docs.forEach((c: any) => {
      sensor_data.push(c.data());
    });

    return (
      <div>
        <div>
          <div>
            <p>
              <label htmlFor="limit">Limit: </label>
              <select
                onChange={handleLimit}
                id="limit"
                name="limit"
                defaultValue={options.limit}
              >
                <option value="12">12 readings (1 hour)</option>
                <option value="24">24 readings (2 hours)</option>
                <option value="48">48 readings (4 hours)</option>
                <option value="96">96 readings (8 hours)</option>
                <option value="288">288 readings (24 hours)</option>
              </select>
              <br />
              <label htmlFor="resolution">Resolution: </label>
              <select
                onChange={handleResolution}
                id="resolution"
                name="resolution"
                defaultValue={divider}
              >
                <option value="5">5 min</option>
                <option value="15">15 min</option>
                <option value="30">30 min</option>
                <option value="60">60 min</option>
              </select>
            </p>
          </div>
        </div>
        <button onClick={handleRefresh}>Apply filter</button>
        <table>
          <thead className="history">
            <tr>
              {/*<th id="sensor_id">Sensor ID</th>*/}
              <th id="temp">Temp &#xb0;C</th>
              <th id="humidity">RH %</th>
              <th id="timestamp">Measure timestamp</th>
            </tr>
          </thead>
          <tbody>
            {renderSensorTableData(sensor_data, dateTimeFormat, divider)}
          </tbody>
        </table>
      </div>
    );
  }
  return (
    <div>
      <button onClick={handleRefresh}>Apply filter</button>
      <p>empty</p>
    </div>
  );
}

type CollectionOptions = {
  document: string | firebase.firestore.FieldPath;
  limit: number;
  direction?: "desc" | "asc";
};
let collectionOptions: CollectionOptions = {
  document: "timestamp",
  limit: 12,
  direction: "desc",
};

type DateMinMax = {
  min: Date;
  max: Date;
};

var renderSensorTableData = (
  sensor_data: string[],
  dateTimeFormat: Intl.DateTimeFormat,
  divider: number
) => {
  return sensor_data.map((values: any, i: number) => {
    const { temp, humidity, timestamp } = values;
    let mins = parseInt(timestamp.slice(14, 16));
    if (mins % divider === 0) {
      return (
        <tr key={i}>
          {/*
        <td>{sensor_id}</td>
        */}
          <td>{temp.toFixed(1)} &#xb0;C</td>
          <td>{humidity.toFixed(1)} %</td>
          <td>{dateTimeFormat.format(new Date(timestamp))}</td>
        </tr>
      );
    } else {
      return null;
    }
  });
};

var applyFilter = (
  options: CollectionOptions,
  doclimit: number,
  date: DateMinMax
) => {
  let db = firebase.firestore(app);
  var docRef = db
    .collection("data")
    .doc("templogger_1")
    .collection("sensor_data")
    .limit(doclimit);

  if (options.document === "timestamp") {
    docRef = db
      .collection("data")
      .doc("templogger_1")
      .collection("sensor_data")
      .orderBy(options.document, options.direction)
      .limit(doclimit)
      .where("timestamp", ">=", date.min.toISOString())
      .where("timestamp", "<=", date.max.toISOString());
    return docRef;
  }
  if (options.document !== "timestamp") {
    docRef = db
      .collection("data")
      .doc("templogger_1")
      .collection("sensor_data")

      .orderBy(options.document, options.direction)
      .limit(doclimit);
    return docRef;
  }
  return docRef;
};
